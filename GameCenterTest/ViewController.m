//
//  ViewController.m
//  GameCenterTest
//
//  Created by MrStock on 19/06/14.
//  Copyright (c) 2014 Thomas Diffendal. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()



// A flag indicating whether the Game Center features can be used after a user has been authenticated.

@property (nonatomic) BOOL gameCenterEnabled;



// This property stores the default leaderboard's identifier.

@property (nonatomic, strong) NSString *leaderboardIdentifier;



// The player's score. Its type is int64_t so as to match the expected type by the respective method of GameKit.

@property (nonatomic) int64_t score;



// First thing you need to do working with Game Center

-(void)authenticateLocalPlayer;



-(void)reportScore:(int64_t)score;



-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard;



@end



@implementation ViewController



- (void)viewDidLoad

{
    
    [super viewDidLoad];
    
    
    
    _gameCenterEnabled = NO;
    
    _leaderboardIdentifier = @"";
    
    
    
    [self authenticateLocalPlayer];
    
}



- (void)didReceiveMemoryWarning

{
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}



-(void)authenticateLocalPlayer{
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        
        if (viewController != nil) {
            
            //if not logged in, show game center login screen
            
            //you should pause your game
            
            [self presentViewController:viewController animated:YES completion:nil];
            
        }
        
        else{
            
            if ([GKLocalPlayer localPlayer].authenticated) {
                
                //You are now logged in
                
                NSLog(@"Local player authenticated");
                
                _gameCenterEnabled = YES;
                
                
                
                // Get the default leaderboard identifier.
                
                [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
                    
                    
                    
                    if (error != nil) {
                        
                        NSLog(@"%@", [error localizedDescription]);
                        
                    }
                    
                    else{
                        
                        _leaderboardIdentifier = leaderboardIdentifier;
                        
                        NSLog(@"Leaderboard ID : %@",_leaderboardIdentifier);
                        
                        
                        
                        if (_gameCenterEnabled) {
                            
                            
                            
                            //report a score on default leaderboard
                            
                            [self reportScore:1000];
                            
                            //Pass YES to show Leaderboard or NO to show Achievements
                            
                            [self showLeaderboardAndAchievements:YES];
                            
                        }
                        
                        
                        
                    }
                    
                }];
                
            }
            
            
            
            else{
                
                //User has cancelled the login screen ?
                
                NSLog(@"Cancel sign in?");
                
                _gameCenterEnabled = NO;
                
            }
            
        }
        
    };
    
}



-(void)reportScore:(int64_t)score{
    
    GKScore *gcscore = [[GKScore alloc] initWithLeaderboardIdentifier:_leaderboardIdentifier];
    
    gcscore.value = score;
    
    NSLog(@"Reporting score to GameCenter");
    
    [GKScore reportScores:@[gcscore] withCompletionHandler:^(NSError *error) {
        
        if (error != nil) {
            
            NSLog(@"%@", [error localizedDescription]);
            
        } else NSLog(@"Reported score : %lli",score);
        
    }];
    
}



-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard{
    
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    
    
    gcViewController.gameCenterDelegate = self;
    
    
    
    if (shouldShowLeaderboard) {
        
        gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
        
        gcViewController.leaderboardIdentifier = _leaderboardIdentifier;
        
    }
    
    else{
        
        gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
        
    }
    
    
    
    [self presentViewController:gcViewController animated:YES completion:nil];
    
}



//Need to dismiss the GameCenterViewController

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController{
    
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
    
}







@end