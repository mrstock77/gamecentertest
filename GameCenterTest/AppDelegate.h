//
//  AppDelegate.h
//  GameCenterTest
//
//  Created by MrStock on 19/06/14.
//  Copyright (c) 2014 Thomas Diffendal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
