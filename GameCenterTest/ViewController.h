//
//  ViewController.h
//  GameCenterTest
//
//  Created by MrStock on 19/06/14.
//  Copyright (c) 2014 Thomas Diffendal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SpriteKit/SpriteKit.h>

#import <GameKit/GameKit.h>



@interface ViewController : UIViewController <GKGameCenterControllerDelegate>



@end

